## HowTo Build

docker build -t nextjs-web-app:1.0.1 .

## HowTo Run in Dev mode

docker-compose -f docker-compose.yaml -f docker-compose.dev.yaml up

## HowTo Run in Prod mode

docker-compose -f docker-compose.yaml -f docker-compose.prod.yaml up

edited from master branch

